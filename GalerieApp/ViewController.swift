//
//  ViewController.swift
//  GalerieApp
//
//  Created by Admin on 01/09/2018.
//  Copyright © 2018 oulmaki. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    // MARK: Outlets
    @IBOutlet weak var randomBtn: UIButton!
    @IBOutlet weak var randomPic: UIImageView!
    @IBOutlet weak var randomDescription: UILabel!
    
    // MARK: Data
    let album = Album()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.setupUI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}


/// MARK: UI
extension ViewController
{
    private func setupUI()
    {
        // UIButton
        self.randomBtn.layer.cornerRadius = 8.0
        self.randomBtn.setTitle("Show me an animal", for: .normal)
        
        // UILabel
        self.randomDescription.backgroundColor = UIColor(red: 127/255.0, green: 140/255.0, blue: 141/255.0, alpha: 0.4)
        self.randomDescription.adjustsFontSizeToFitWidth = true
        
        // first image
        self.showRandomPicture()
    }
}

/// MARK: IBActions
extension ViewController
{
    @IBAction func showRandomPicturePressed()
    {
        self.showRandomPicture()
    }
}

/// MARK: Random picture
extension ViewController
{
    private func showRandomPicture()
    {
        let randomPic = self.album.getRandomPicture()
        self.randomPic.image = UIImage(named: randomPic.name)
        if let description = randomPic.description
        {
            self.randomDescription.text = description
        }
    }
}
