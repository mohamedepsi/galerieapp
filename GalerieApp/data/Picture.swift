//
//  Picture.swift
//  GalerieApp
//
//  Created by Admin on 02/09/2018.
//  Copyright © 2018 oulmaki. All rights reserved.
//

import Foundation
import GameplayKit

struct Picture
{
    let name:String
    let description: String?
}

struct Album
{
    let pictures = [
        Picture(name: "monkey", description: "Monkeys are non-hominoid simians, generally possessing tails and consisting of about 260 known living species. Many monkey species are tree-dwelling (arboreal), although there are species that live primarily on the ground, such as baboons. Most species are also active during the day (diurnal). Monkeys are generally considered to be intelligent, particularly Old World monkeys."),
        
        Picture(name: "tiger", description: "The tiger (Panthera tigris) is the largest cat species, most recognizable for its pattern of dark vertical stripes on reddish-orange fur with a lighter underside. The species is classified in the genus Panthera with the lion, leopard, jaguar and snow leopard. It is an apex predator, primarily preying on ungulates such as deer and bovids."),
        
        Picture(name: "giraffe", description: "The giraffe (Giraffa) is a genus of African even-toed ungulate mammals, the tallest living terrestrial animals and the largest ruminants. The genus currently consists of one species, Giraffa camelopardalis, the type species. Seven other species are extinct, prehistoric species known from fossils."),
        
        Picture(name: "parrot", description: "Parrots, also known as psittacines /ˈsɪtəsaɪnz/,[1][2] are birds of the roughly 393 species in 92 genera that make up the order Psittaciformes, found in most tropical and subtropical regions. The order is subdivided into three superfamilies: the Psittacoidea, the Cacatuoidea (cockatoos), and the Strigopoidea (New Zealand parrots). "),
        
        Picture(name: "zebra", description: "Zebras (/ˈziːbrə/ ZEE-brə, UK also /ˈzɛbrə/ ZEB-rə)[1] are several species of African equids (horse family) united by their distinctive black and white striped coats. Their stripes come in different patterns, unique to each individual. They are generally social animals that live in small harems to large herds. Unlike their closest relatives, horses and donkeys, zebras have never been truly domesticated."),
        
        Picture(name: "lion", description: "The lion (Panthera leo) is a species in the cat family (Felidae); it is a muscular, deep-chested cat with a short, rounded head, a reduced neck and round ears, and a hairy tuft at the end of its tail. The lion is sexually dimorphic; males are larger than females with a typical weight range of 150 to 250 kg (330 to 550 lb) for the former and 120 to 182 kg (265 to 400 lb) for the latter. Male lions have a prominent mane, which is the most recognisable feature of the species. A lion pride consists of a few adult males, related females and cubs."),
        
        Picture(name: "koala", description: "The koala (Phascolarctos cinereus, or, inaccurately, koala bear[a]) is an arboreal herbivorous marsupial native to Australia. It is the only extant representative of the family Phascolarctidae and its closest living relatives are the wombats. The koala is found in coastal areas of the mainland's eastern and southern regions, inhabiting Queensland, New South Wales, Victoria, and South Australia. It is easily recognisable by its stout, tailless body and large head with round, fluffy ears and large, spoon-shaped nose. The koala has a body length of 60–85 cm (24–33 in) and weighs 4–15 kg (9–33 lb). Pelage colour ranges from silver grey to chocolate brown. ")
        
    ]
    
    func getRandomPicture() -> Picture {
        let randomNumber = GKRandomSource.sharedRandom().nextInt(upperBound: pictures.count)
        return pictures[randomNumber]
    }
}
